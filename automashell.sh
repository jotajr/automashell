#!/bin/bash
# ==============================================
# AutomaShell - Servidores GNU/Linux Debian Like
# ==============================================
#
# Instituto Federal de Educação Ciência e Tecnologia - IFPB
# Campus Campina Grande
# Curso Superior de Tecnologia em Telemática
#
# Trabalho de Conclusão de Curso - Segurança e Administração de Servidores Linux
# Jeosafá de Melo Freitas Júnior - Matrícula: 20072121022
#
# Este Script está sob licença GNU V3 disponível em: http://www.gnu.org/licenses/gpl-3.0.txt
#
# CÓDIGO FONTE DISPONÍVEL EM: https://code.google.com/p/automashell/ e em https://github.com/jotajr/automashell.git
#
#=============================================
# Método de Versionamento
# VERSÃO X.X.X
#        | | |
#        | |  --> Pequenas Melhorias
#        |  --> Melhorias Significativas
#         --> Totalmente Funcional
#=============================================
#
# Versão 0.0.1 - Início da Codificação
# Versão 0.0.2 - Tratamentos de Mensagens de linha de comando
# Versão 0.0.3 - Captura a Versão diretamento do cabeçalho
# Versão 0.0.4 - Modularização através de Funções
# Versão 0.0.5 - Adicionando Menus
# Versão 0.1.0 - Adicionando arquivo de LOG
# Versão 0.1.1 - Adicionando Função de instalação do LAMP + PhpMyAdmin
# Versão 0.1.2 - Adicionando Função de instalação do Webmin
# Versão 0.1.3 - Adicionando Função de instalação do PostFix
# Versão 0.2.0 - Adicionando opção -c ou --changelog para mostrar o changelog da aplicação
# Versão 1.0.0 - Primeira Versão Totalmente funcional
# Versão 1.0.1 - Adicionado a remoção para a função nmapMenu
# Versão 1.0.2 - Adicionado a remoção para a função rootkitMenu
# Versão 1.0.3 - Alterado o layout dos arquivos de log
# Versão 1.1.0 - Adicionado log para a varredura de portas com o NMAP
#

########## DECLARAÇÕES DE FUNÇÕES ##########

#Função 00 - Verifica se um programa está instalado ou não sistema
function verificaInstall ()
{   
	if [ $(dpkg --get-selections | grep -w $1 | wc -l) -eq 0 ]
	then
		return 0 #Retorna 0 se o programa não estiver instalado
	else
		return 1 # Retorna 1 caso contrário
	fi
}

#Função 01 - Verifica se o Usuário é ROOT
function verificaRoot(){
	if [ "$(id -u)" != "0" ]
		then 
	MENSAGEM_ERRO_ROOT="
ERRO DE EXECUÇÃO:
Este programa só poderá executar todas as suas funcionalidades no modo super-usuário.
Por favor, faça login com o super usuário e execute o $(basename "$0") novamente.
"
		echo "$MENSAGEM_ERRO_ROOT"
		echo "ERRO - "`date +%d/%m/%Y-%H:%M:%S`" - Usuário não é ROOT" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
		exit 1
fi
}

#Função 02 - Verifica se o Dialog está instalado no sistema
function verificaDialog(){
	verificaInstall dialog
	if [ $? -eq 0 ]
	then
		MENSAGEM_ERRO_DIALOG="
O aplicativo Dialog é necessário à execução e não está instalado no seu sistema.
Sem ele não é possível executar esta aplicação. Deseja instalar o dialog para prosseguir com a execução?[Y/n]
"
		echo "$MENSAGEM_ERRO_DIALOG"
		read resposta
		if [ $resposta = "Y"  -o $resposta = "y" ]
		then
			echo "Iniciando a Instalação do Dialog!"
			apt-get -y install dialog
			echo "INSTALL - "`date +%d/%m/%Y-%H:%M:%S`" - INSTALOU DIALOG" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
			dialog --title 'Sucesso na Instalação' --sleep 3 --infobox 'Dialog instalado com sucesso!\nAguarde...\nIniciando a Aplicação ...\n' 0 0
		else
			MENSAGEM_ERRO_DIALOG="
ERRO DE EXECUÇÃO:			
Você escolheu não instalar o Dialog, se desejar executar o programa novamente,
responda Y para a pergunta anterior para a instalação do Dialog e a correta execução deste programa.
Saindo da Aplicação
"
			echo "$MENSAGEM_ERRO_DIALOG"
			echo "ERRO - "`date +%d/%m/%Y-%H:%M:%S`" - Dialog Não Instalado" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
			exit 1
		fi
	else
		echo "Dialog instalado"
	fi
}

#Função 03 - Atualiza repositório local
function autalizaRepo() {
clear
echo -e "\n\n ========== Atualizando o Repositório Local ========== \n\n"
apt-get update
dialog --stdout  \
	--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
	--beep \
	--title "Sucesso" \
	--msgbox "O Repositório esta atualizado!" 0 0
echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Repositório Local do Sistema Atualizado" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
}

#Função 04 - Sobre o programa
function sobre() {
dialog --stdout  \
	--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
	--cr-wrap \
	--title "Sobre o Sistema" \
	--msgbox "
	================= AUTOMASHELL =================
	
	Instituto Federal de Educação Ciência e Tecnologia da Paraíba- IFPB
	Campus Campina Grande
	Curso Superior de Tecnologia em Telemática

	Trabalho de Conclusão de Curso
	Jeosafá de Melo Freitas Júnior - Matrícula: 20072121022
	jotafreitasjr@gmail.com
	
	===============================================
	Código Disponível em:
	https://code.google.com/p/automashell/
	https://github.com/jotajr/automashell.git
	" 0 0
echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Mostrando a Tela Sobre o Sistema" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
}

#Função 05 - Instalação OpenSSH-Server
function setOpenssh {
	while : ; do
		escolha_ssh=$(
		dialog --stdout  \
		--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
		--ok-label "Selecionar" --cancel-label "Voltar ao Menu Anterior" \
		--title "OPENSSH-SERVER - Servidor SSH" \
		--menu "Escolha uma das opções abaixo:" \
		0 0 0 \
		1 'Instalar o SSHSEREVR - Servidor SSH' \
		2 'Remover o SSHSEREVR - Servidor SSH')
		
		#Se Usuário pressionar ESC ou selecionar 'Voltar' o programa volta para a tela anterior
		[ $? -ne 0 ] && break
						
		case "$escolha_ssh" in
				
			1)
				clear
				echo -e "\n\n ========== Instalando/Verificando o OPENSSH-SERVER ========== \n\n"
				apt-get -y install openssh-server
				dialog --stdout  \
				--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
				--beep \
				--title "Sucesso" \
				--msgbox "O SSHSERVER já está instalado em seu sistema!" 0 0
				echo "INSTALL - "`date +%d/%m/%Y-%H:%M:%S`" - Instalando/Verificando OpenSSH-Server" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				;;
				
			2)
				verificaInstall openssh-server
				if [ $? -eq 0 ]
				then
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Remover SSHSERVER" \
					--msgbox "O OpenSSH-Server não está instalado no se sistema!" \
					0 0	
					echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Tentativa de Remover programa não Instalado(OPENSSH-SERVER)" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				else
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Remover SSHSERVER" \
					--yesno "Deseja realmente remover o OpenSSH-Server do seu sistema?" \
					0 0
					if [ $? = 0 ]; then
						clear
						echo -e "\n\n ========== REMOVENDO o OPENSSH-SERVER ========== \n\n"
						apt-get -y --purge remove openssh-server
						echo "REMOVE - "`date +%d/%m/%Y-%H:%M:%S`" - OpenSSH-Server Removido do Sistema" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
						apt-get -y autoremove
						dialog --stdout  \
						--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
						--title "Remover SSHSERVER" \
						--msgbox "O OpenSSH-Server foi removido com sucesso do sistema" \
						0 0
					else
						dialog --stdout  \
						--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
						--title "Remover SSHSERVER" \
						--msgbox "Ok, o OpenSSH-Server continua instalado no sistema!" \
						0 0
						echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Usuário cancelou a operação de remoção do OpenSSH-Server" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
					fi
				fi				
				;;	
		esac	
	done
}

#Função 06 - Instalação LAMP
function setLAMPP {
	while : ; do
		escolha_lamp=$(
		dialog --stdout  \
		--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
		--ok-label "Selecionar" --cancel-label "Voltar ao Menu Anterior" \
		--title "Apache - MySQL - PHP - PhpMyAdmin" \
		--menu "Escolha uma das opções abaixo:" \
		0 0 0 \
		1 'Instalar o LAMP - Servidor Web Completo' \
		2 'Instalar o PHPMyAdmin' \
		3 'Remover o LAMP - Servidor Web Completo' \
		4 'Remover o PhpMyAdmin')
		
		#Se Usuário pressionar ESC ou selecionar 'Voltar' o programa volta para a tela anterior
		[ $? -ne 0 ] && break
						
		case "$escolha_lamp" in
				
			1)
				clear
				echo -e "\n\n ========== Instalando/Verificando o LAMP SERVER ========== \n\n"
				echo -e "\n\n ========== Instalando/Verificando APACHE2 ========== \n\n"
				apt-get -y install apache2
				echo "INSTALL - "`date +%d/%m/%Y-%H:%M:%S`" - LAMP: Apache2 Instalado" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"	
				echo -e "\n\n ========== Instalando/Verificando PHP5 ========== \n\n"
				apt-get -y install php5 php5-cli libapache2-mod-php5 php5-cgi php5-mysql
				echo "INSTALL - "`date +%d/%m/%Y-%H:%M:%S`" - LAMP: PHP5 Instalado" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				echo "<?php phpinfo(); ?>" > /var/www/info.php
				echo -e "\n\n ========== Instalando/Verificando MySQL ========== \n\n"
				apt-get -y install mysql-server mysql-client
				echo "INSTALL - "`date +%d/%m/%Y-%H:%M:%S`" - LAMP: MySQL Instalado" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				/etc/init.d/apache2 restart
				echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - LAMP: Reiniciando Apache web server" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				dialog --stdout  \
				--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
				--beep \
				--title "Sucesso" \
				--msgbox "O LAMPP já está instalado em seu sistema!\nAcesse http://ipdoservidor para a verificação do funcionamento do Apache2"  0 0
				dialog --stdout  \
				--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
				--beep \
				--title "Sucesso" \
				--msgbox "Acesse: http://ipdoservidor/info.php para informações sobre o PHP5"  0 0
				echo "INSTALL - "`date +%d/%m/%Y-%H:%M:%S`" - Instalado LAMP Server" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				;;
				
			2) 
				clear
				echo -e "\n\n ========== Instalando/Verificando o PHPMYADMIN ========== \n\n"				
				apt-get -y install php5-mcrypt phpmyadmin
				ln -s /usr/share/phpmyadmin /var/www/phpmyadmin
				echo "LAMPP - "`date +%d/%m/%Y-%H:%M:%S`" - PHPMYADMIN Instalado" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				dialog --stdout  \
				--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
				--beep \
				--title "Sucesso" \
				--msgbox "Acesse: http://ipdoservidor/phpmyadmin para utilizar o phpmyadmin"  0 0
				echo "INSTALL - "`date +%d/%m/%Y-%H:%M:%S`" - Instalado PHPMYADMIN" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				;;
				
			3)
				dialog --stdout  \
				--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
				--title "Remover LAMP Server" \
				--yesno "Deseja realmente remover o LAMP Server do seu sistema?" \
				0 0
				if [ $? = 0 ]; then
					clear
					echo -e "\n\n ========== REMOVENDO o LAMP Server ========== \n\n"
					/etc/init.d/apache2 stop
					apt-get -y --purge remove apache2 php5 php5-cli libapache2-mod-php5 php5-cgi php5-mysql php5-mcrypt mysql-server mysql-client
					rm /var/www/info.php					
					echo "REMOVE - "`date +%d/%m/%Y-%H:%M:%S`" - LAMP Server Removido do Sistema" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
					apt-get -y autoremove
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Remover LAMP Server" \
					--msgbox "O LAMP Server foi removido com sucesso do sistema" \
					0 0
				else
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Remover LAMP Server" \
					--msgbox "Ok, o LAMP Server continua instalado no sistema!" \
					0 0
					echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Usuário cancelou a operação de remoção do LAMP Server" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				fi								
				;;	
				
			4)
				dialog --stdout  \
				--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
				--title "Remover PHPMYADMIN" \
				--yesno "Deseja realmente remover o PhpMyAdmin do seu sistema?" \
				0 0
				if [ $? = 0 ]; then
					clear
					echo -e "\n\n ========== REMOVENDO o PhpMyAdmin ========== \n\n"
					apt-get -y --purge remove phpmyadmin
					rm /var/www/phpmyadmin
					echo "REMOVE - "`date +%d/%m/%Y-%H:%M:%S`" - PhpMyAdmin Removido do Sistema" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
					apt-get -y autoremove
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Remover PhpMyAdmin" \
					--msgbox "O PhpMyAdmin foi removido com sucesso do sistema" \
					0 0
				else
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Remover PhpMyAdmin" \
					--msgbox "Ok, o PhpMyAdmin continua instalado no sistema!" \
					0 0
					echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Usuário cancelou a operação de remoção do PhpMyAdmin" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				fi								
				;;	
				
		esac	
	done
}

# Função 07 - Instalação do Postfix
function setPostfix {
	while : ; do
		escolha_postfix=$(
		dialog --stdout  \
		--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
		--ok-label "Selecionar" --cancel-label "Voltar ao Menu Anterior" \
		--title "POSTFIX - Servidor de E-mails" \
		--menu "Escolha uma das opções abaixo:" \
		0 0 0 \
		1 'Instalar o POSTFIX' \
		2 'Remover o POSTFIX')
		
		#Se Usuário pressionar ESC ou selecionar 'Voltar' o programa volta para a tela anterior
		[ $? -ne 0 ] && break
						
		case "$escolha_postfix" in
				
			1)
				clear
				echo -e "\n\n ========== Instalando/Verificando o POSTFIX ========== \n\n"
				apt-get -y install postfix
				dialog --stdout  \
				--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
				--beep \
				--title "Sucesso" \
				--msgbox "O POSTFIX já está instalado em seu sistema!" 0 0
				echo "INSTALL - "`date +%d/%m/%Y-%H:%M:%S`" - Instalando/Verificando Postfix" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				;;
				
			2)
				verificaInstall postfix
				if [ $? -eq 0 ]
				then
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Remover POSTFIX" \
					--msgbox "O Postfix não está instalado no se sistema!" \
					0 0	
					echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Tentativa de Remover programa não Instalado(POSTFIX)" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				else
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Remover POSTFIX" \
					--yesno "Deseja realmente remover o Postfix do seu sistema?" \
					0 0
					if [ $? = 0 ]; then
						clear
						echo -e "\n\n ========== REMOVENDO o POSTFIX E-mail Server ========== \n\n"
						apt-get -y --purge remove postfix
						echo "REMOVE - "`date +%d/%m/%Y-%H:%M:%S`" - Postfix E-Mail Server Removido do Sistema" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
						apt-get -y autoremove
						dialog --stdout  \
						--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
						--title "Remover POSTFIX" \
						--msgbox "O Postfix E-Mail Server foi removido com sucesso do sistema" \
						0 0
					else
						dialog --stdout  \
						--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
						--title "Remover POSTFIX" \
						--msgbox "Ok, o Postfix E-Mail Server continua instalado no sistema!" \
						0 0
						echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Usuário cancelou a operação de remoção do Postfix E-Mail Server" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
					fi
				fi				
				;;	
		esac	
	done
}

# Função 08 - Instalação do Tomcat
function setTomcat {
	while : ; do
		escolha_tomcat=$(
		dialog --stdout  \
		--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
		--ok-label "Selecionar" --cancel-label "Voltar ao Menu Anterior" \
		--title "Tomcat 7 - Container Java Web" \
		--menu "Escolha uma das opções abaixo:" \
		0 0 0 \
		1 'Instalar o Tomcat 7 - Container Java Web' \
		2 'Remover o Tomcat 7 - Container Java Web')
		
		#Se Usuário pressionar ESC ou selecionar 'Voltar' o programa volta para a tela anterior
		[ $? -ne 0 ] && break
						
		case "$escolha_tomcat" in
				
			1)
				clear
				echo -e "\n\n ========== Instalando/Verificando o TOMCAT 7 ========== \n\n"
				apt-get -y install tomcat7 tomcat7-admin 
				dialog --stdout  \
				--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
				--beep \
				--title "Sucesso" \
				--msgbox "O Tomcat 7 já está instalado em seu sistema!\nAcesse: http://ip_do_servidor:8080" 0 0
				echo "INSTALL - "`date +%d/%m/%Y-%H:%M:%S`" - Instalando/Verificando Tomcat 7" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				;;
				
			2)
				verificaInstall tomcat7
				if [ $? -eq 0 ]
				then
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Remover Tomcat 7" \
					--msgbox "O Tomcat 7 não está instalado no se sistema!" \
					0 0	
					echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Tentativa de Remover programa não Instalado(Tomcat 7)" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				else
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Remover Tomcat 7" \
					--yesno "Deseja realmente remover o Tomcat 7 do seu sistema?" \
					0 0
					if [ $? = 0 ]; then
						clear
						echo -e "\n\n ========== REMOVENDO o Tomcat 7 - Container Java Web ========== \n\n"
						apt-get -y --purge remove tomcat7 tomcat7-admin
						echo "REMOVE - "`date +%d/%m/%Y-%H:%M:%S`" - Tomcat 7 - Container Java Web Removido do Sistema" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
						apt-get -y autoremove
						dialog --stdout  \
						--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
						--title "Remover Tomcat 7" \
						--msgbox "Tomcat 7 - Container Java Web foi removido com sucesso do sistema" \
						0 0
					else
						dialog --stdout  \
						--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
						--title "Remover Tomcat 7" \
						--msgbox "Ok, o Tomcat 7 - Container Java Web continua instalado no sistema!" \
						0 0
						echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Usuário cancelou a operação de remoção do Tomcat 7 - Container Java Web" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
					fi
				fi				
				;;	
		esac	
	done
}

#Função 09 - Instalação Oracle JDK 7
function setJava {
	while : ; do
		escolha_java=$(
		dialog --stdout  \
		--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
		--ok-label "Selecionar" --cancel-label "Voltar ao Menu Anterior" \
		--title "Java 7 - Oracle JDK7" \
		--menu "Escolha uma das opções abaixo:" \
		0 0 0 \
		1 'Instalar o Java 7 - Oracle JDK7' )
		
		#Se Usuário pressionar ESC ou selecionar 'Voltar' o programa volta para a tela anterior
		[ $? -ne 0 ] && break
						
		case "$escolha_java" in
				
			1)
				verificaInstall oracle-java7-installer
				if [ $? -eq 0 ]
				then
					clear
					echo -e "\n\n ========== Instalando/Verificando o Java 7 - Oracle JDK7 ========== \n\n"
					#Adiciona ao repositório a fonte para o Oracle Java 7
					add-apt-repository -y ppa:webupd8team/java
					autalizaRepo
					apt-get -y install oracle-java7-installer
					echo "JDK7 - "`date +%d/%m/%Y-%H:%M:%S`" - webupd8team/java incluído no repositório" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--beep \
					--title "Sucesso" \
					--msgbox "O JDK 7 está instalado em seu sistema!" 0 0
					echo "INSTALL - "`date +%d/%m/%Y-%H:%M:%S`" - Instalando/Verificando Tomcat 7" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				else
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--beep \
					--title "Sucesso" \
					--msgbox "O JDK 7 já está instalado em seu sistema!" 0 0
				fi
				;;
				
		esac	
	done
	
}

# Função 10 - Instalação do Webmin
function setWebmin {
	while : ; do
		escolha_webmin=$(
		dialog --stdout  \
		--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
		--ok-label "Selecionar" --cancel-label "Voltar ao Menu Anterior" \
		--title "WEBMIN - Gerência de Servidores GNU/Linux" \
		--menu "Escolha uma das opções abaixo:" \
		0 0 0 \
		1 'Instalar o Webmin' \
		3 'Remover o Webmin')
		
		#Se Usuário pressionar ESC ou selecionar 'Voltar' o programa volta para a tela anterior
		[ $? -ne 0 ] && break
						
		case "$escolha_webmin" in
				
			1)	
				verificaInstall webmin
				if [ $? -eq 0 ]
				then
					clear
					echo -e "\n\n ========== Instalando/Verificando o Webmin ========== \n\n"
					if [ $(cat /etc/apt/sources.list | grep -w webmin | wc -l) -eq 0 ]
					then
						echo "#REPOSITORIO WEBMIN" >> /etc/apt/sources.list
						echo "deb http://download.webmin.com/download/repository sarge contrib" >> /etc/apt/sources.list
						echo "deb http://webmin.mirror.somersettechsolutions.co.uk/repository sarge contrib" >> /etc/apt/sources.list
						wget http://www.webmin.com/jcameron-key.asc
						apt-key add jcameron-key.asc
						rm jcameron-key.asc
						autalizaRepo
						echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Webmin incluído no sources.list" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
						apt-get -y install webmin
						dialog --stdout  \
						--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
						--beep \
						--title "Sucesso" \
						--msgbox "O Webmin já está instalado em seu sistema!\nPara utilizar acesse: https://ip_do_servidor:10000" 0 0
						echo "INSTALL - "`date +%d/%m/%Y-%H:%M:%S`" - Instalação do Webmin Concluída através do repositório" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
					fi
					apt-get -y install webmin
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--beep \
					--title "Sucesso" \
					--msgbox "O Webmin já está instalado em seu sistema!\nPara utilizar acesse: https://ip_do_servidor:10000" 0 0
					echo "INSTALL - "`date +%d/%m/%Y-%H:%M:%S`" - Instalação do Webmin Concluída através do repositório" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				else
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--beep \
					--title "Sucesso" \
					--msgbox "O Webmin já está instalado em seu sistema!\nPara utilizar acesse: https://ip_do_servidor:10000" 0 0
					echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Tentativa de instalação do Webmin(Já Instalado)." >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				fi	
				;;
			
			2)
				verificaInstall webmin
				if [ $? -eq 0 ]
				then
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Remover WEBMIN" \
					--msgbox "O WEBMIN não está instalado no se sistema!" \
					0 0	
					echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Tentativa de Remover programa não Instalado(WEBMIN)" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				else
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Remover WEBMIN" \
					--yesno "Deseja realmente remover o WEBMIN do seu sistema?" \
					0 0
					if [ $? = 0 ]; then
						clear
						echo -e "\n\n ========== REMOVENDO o WEBMIN E-mail Server ========== \n\n"
						apt-get -y --purge remove webmin
						echo "REMOVE - "`date +%d/%m/%Y-%H:%M:%S`" - WEBMIN Removido do Sistema" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
						apt-get -y autoremove
						dialog --stdout  \
						--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
						--title "Remover WEBMIN" \
						--msgbox "O WEBMIN foi removido com sucesso do sistema" \
						0 0
					else
						dialog --stdout  \
						--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
						--title "Remover WEBMIN" \
						--msgbox "Ok, o WEBMIN Server continua instalado no sistema!" \
						0 0
						echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Usuário cancelou a operação de remoção do WEBMIN" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
					fi
				fi				
				;;	
		esac	
	done
}

#Função 11 - Menu NMAP
function nmapMenu(){
	while : ; do
		escolha_nmap=$(
		dialog --stdout  \
		--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
		--ok-label "Selecionar" --cancel-label "Voltar ao Menu Anterior" \
		--title "NMAP - Port Scanner" \
		--menu "Escolha uma das opções abaixo:" \
		0 0 0 \
		1 'Instalar o NMAP - Port Scanner' \
		2 'Varrer portas de um determinado Host' \
		3 'Remover o NMPA - PortScanner' )
		
		#Se Usuário pressionar ESC ou selecionar 'Voltar' o programa volta para a tela anterior
		[ $? -ne 0 ] && break
						
		case "$escolha_nmap" in
				
			1)
				clear
				echo -e "\n\n ========== Instalando/Verificando o NMAP ========== \n\n"
				apt-get -y install nmap
				dialog --stdout  \
				--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
				--beep \
				--title "Sucesso" \
				--msgbox "O NMAP já está instalado em seu sistema!" 0 0
				echo "INSTALL - "`date +%d/%m/%Y-%H:%M:%S`" - Instalando/Verificando NMAP" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				;;
				
			2)
				NMAP=$(dpkg --get-selections | grep nmap | wc -l)
				if [ $NMAP -eq 0 ]
				then
					echo "ERRO - "`date +%d/%m/%Y-%H:%M:%S`" - Varredura sem o NMAP está instalado" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Impossível fazer a Varredura" \
					--msgbox "O NMAP ainda não está instalado no seu sistema\nUtilize a tela anterior para a instalação" \
					0 0									
				else								
					ip=$(
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "NMAP - Varredura de Portas" \
					--inputbox "Digite o IP do host (Para esta máquina digite "localhost"):" \
					0 0)
					echo "SCAN - "`date +%d/%m/%Y-%H:%M:%S`" - NMAP - Varredura no IP: $ip" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
					touch "AUTOMASHELL_NMAP_"`date +%Y%m%d%H`".log"
					clear
					echo "Aguarde a Varredura do IP: $ip"
					nmap "$ip" >> "AUTOMASHELL_NMAP_"`date +%Y%m%d%H`".log"
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "NMAP - Resultado da Varredura no IP: $ip" \
					--textbox "AUTOMASHELL_NMAP_"`date +%Y%m%d%H`".log" \
					50 0	
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Varredura Finalizada!" \
					--yesno "Deseja apagar o arquivo de log gerado para o NMAP?" \
					0 0
					if [ $? = 0 ]; then
						rm "AUTOMASHELL_NMAP_"`date +%Y%m%d%H`".log"
						dialog --stdout  \
						--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
						--title "Log do NMAP" \
						--msgbox "Arquivo de LOG APAGADO!" \
						0 0
					else
						dialog --stdout  \
						--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
						--title "Log do NMAP" \
						--msgbox "Arquivo de LOG mantido" \
						0 0
						echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - NMAP - Arquivo de Log de Varredura Salvo" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
					fi
				fi
				;;

			3)
				verificaInstall nmap
				if [ $? -eq 0 ]
				then
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Remover NMAP" \
					--msgbox "O NMAP - Port Scanner não está instalado no se sistema!" \
					0 0	
					echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Tentativa de Remover programa não Instalado(NMAP)" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				else
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Remover NMAP" \
					--yesno "Deseja realmente remover o NMAP - Port Scanner do seu sistema?" \
					0 0
					if [ $? = 0 ]; then
						clear
						echo -e "\n\n ========== REMOVENDO o NMAP ========== \n\n"
						apt-get -y --purge remove nmap
						echo "REMOVE - "`date +%d/%m/%Y-%H:%M:%S`" - NMAP - Port Scanner Removido do Sistema" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
						apt-get -y autoremove
						dialog --stdout  \
						--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
						--title "Remover NMAP" \
						--msgbox "O NMAP - Port Scanner foi removido com sucesso do sistema" \
						0 0
					else
						dialog --stdout  \
						--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
						--title "Remover NMAP" \
						--msgbox "Ok, o NMAP - Port Scanner continua instalado no sistema!" \
						0 0
						echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Usuário cancelou a operação de remoção do NMAP - Port Scanner" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
					fi
				fi				
				;;		
		esac	
	done
}

# Função 12 - Menu CHKROOTKIT
function rootkitMenu() {
	while : ; do
		escolha_rootkit=$(
		dialog --stdout  \
		--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
		--ok-label "Selecionar" --cancel-label "Voltar ao Menu Anterior" \
		--title "CHKROOTKIT - Varre o sistema em Busca de RootKit" \
		--menu "Escolha uma das opções abaixo:" \
		0 0 0 \
		1 'Instalar o CHKROOTKIT - Rootkit Scanner' \
		2 'Varrer o sistema em busca de rootkits' \
		3 'Remover o CHKROOTKIT - Rootkit Scanner' )
		
		#Se Usuário pressionar ESC ou selecionar 'Voltar' o programa volta para a tela anterior
		[ $? -ne 0 ] && break
						
		case "$escolha_rootkit" in
				
			1)
				clear
				echo -e "\n\n ========== Instalando/Verificando o CHKROOTKIT ========== \n\n"
				apt-get -y install chkrootkit
				dialog --stdout  \
				--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
				--beep \
				--title "Sucesso" \
				--msgbox "O CHKROOTKIT já está instalado em seu sistema!" 0 0
				echo "INSTALL - "`date +%d/%m/%Y-%H:%M:%S`" - Instalando/Verificando CHKROOTKIT" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				;;
				
			2)
				ROOTKIT=$(dpkg --get-selections | grep chkrootkit | wc -l)
				if [ $ROOTKIT -eq 0 ]
				then
					echo "ERRO - "`date +%d/%m/%Y-%H:%M:%S`" - Varredura no sistema sem o CHKROOTKIT está instalado" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Impossível fazer a Varredura no Sistema" \
					--msgbox "O CHKROOTKIT ainda não está instalado no seu sistema\nUtilize a tela anterior para a instalação" \
					0 0									
				else													
					echo "SCAN - "`date +%d/%m/%Y-%H:%M:%S`" - Varredura no Sistema em busca de Rootkits" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
					touch "AUTOMASHELL_ROOTKIT_"`date +%Y%m%d%H`".log"
					clear
					echo "Aguarde a Varredura do Sistema - CHKROOTKIT"
					chkrootkit >> "AUTOMASHELL_ROOTKIT_"`date +%Y%m%d%H`".log"
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "CHKROOTKIT - Resultado da Varredura" \
					--textbox "AUTOMASHELL_ROOTKIT_"`date +%Y%m%d%H`".log" \
					50 0					
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Varredura Finalizada!" \
					--yesno "Deseja apagar o arquivo de log gerado para o CHKROOTKIT?" \
					0 0
					if [ $? = 0 ]; then
						rm "AUTOMASHELL_ROOTKIT_"`date +%Y%m%d%H`".log"
						dialog --stdout  \
						--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
						--title "Log do CHKROOTKIT" \
						--msgbox "Arquivo de LOG APAGADO!" \
						0 0
					else
						dialog --stdout  \
						--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
						--title "Log do CHKROOTKIT" \
						--msgbox "Arquivo de LOG mantido" \
						0 0
						echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - CHKROOTKIT - Arquivo de Log de Varredura Salvo" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
					fi
				fi
				;;	
				
			3)
				verificaInstall chkrootkit
				if [ $? -eq 0 ]
				then
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Remover CHKROOTKIT" \
					--msgbox "O CHKROOTKIT - Rootkit Scanner não está instalado no se sistema!" \
					0 0	
					echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Tentativa de Remover programa não Instalado(CHKROOTKIT)" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
				else
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--title "Remover CHKROOTKIT" \
					--yesno "Deseja realmente remover o CHKROOTKIT - Rootkit Scanner do seu sistema?" \
					0 0
					if [ $? = 0 ]; then
						clear
						echo -e "\n\n ========== REMOVENDO o CHKROOTKIT ========== \n\n"
						apt-get -y --purge remove chkrootkit
						echo "REMOVE - "`date +%d/%m/%Y-%H:%M:%S`" - NMAP - Port Scanner Removido do Sistema" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
						apt-get -y autoremove
						dialog --stdout  \
						--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
						--title "Remover CHKROOTKIT" \
						--msgbox "O CHKROOTKIT - Rootkit Scanner foi removido com sucesso do sistema" \
						0 0
					else
						dialog --stdout  \
						--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
						--title "Remover CHKROOTKIT" \
						--msgbox "Ok, o CHKROOTKIT - Rootkit Scanner continua instalado no sistema!" \
						0 0
						echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Usuário cancelou a operação de remoção do CHKROOTKIT - Rootkit Scanner" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
					fi
				fi				
				;;
				
		esac	
	done
}

########## FIM DAS DECLARAÇÕES DE FUNÇÕES ##########

########## INÍCIO DA EXECUÇÃO ##########

#Verifica/Cria um arquivo de Log para a execução do Programa
if test -f "AUTOMASHELL_"`date +%Y%m%d%H`".log"
then
	echo "EXEC - "`date +%d/%m/%Y-%H:%M:%S`" - NOVA EXECUÇÃO DO PROGRAMA " >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
else 
	touch "AUTOMASHELL_"`date +%Y%m%d%H`".log"
fi

#Verifica se o usuário é root

verificaRoot

#Tratando Opções da linha de Comando

MENSAGEM_USO="
Uso: $(basename "$0") [-h | -v | -c]

-h , --help			Mostra essa ajuda e sai
-v , --version		Mostra a versão do programa
-c , --changelog	Mostra o Changelog do programa
"

case "$1" in
	-h | --help)
		echo "$MENSAGEM_USO"
		exit 0
	;;
	
	-v | --version)
		echo -n $(basename "$0") -
		#Extrai a versão diretamante do cabeçalho do programa
		grep '^# Versão ' "$0" | tail -1 | cut -d - -f 1 | tr -d \#
		exit 0
	;;
	
	-c | --changelog)
		echo "Change Log da Aplicação $(basename "$0"):"
		cat `echo $0` | grep '^# Versão'
		exit 0
	;;
	
	*)
		if test -n "$1"
		then
			echo Opção Inválida: $1
			echo "Escolha '$(basename "$0") -h' ou '$(basename "$0") --help' para obter ajuda"
			exit 1
		fi
	;;
esac

#Verifica se o dialog está instalado

verificaDialog

#Processamento

dialog --backtitle "AutomaShell - Servidores GNU/Linux Debian Like"  \
	--msgbox "Bem vindo ao AutomaShell\nAplicativo para administração de Servidores GNU/Linux" 0 0

echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Iniciando a Execução do Script" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
	
while : ; do
	# Menu Inicial do Programa
	escolha=$(
		dialog --stdout  \
			--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
			--ok-label "Selecionar" --cancel-label "Sair do Programa" \
			--title "Menu Inicial" \
			--menu "Bem vindo, escolha uma categoria abaixo:" \
			0 0 0 \
			1 'Instalação de Serviços/Aplicações no Servidor' \
			2 'Instalar/Utilizar de Ferramentas de Segurança' \
			3 'Atualizar o Repositório Local' \
			4 'Sobre o Programa' \
			5 'Sair do Programa' )
			
	#Se Usuário pressionar ESC ou selecionar 'Cancelar' o programa encerra
	[ $? -ne 0 ] && break
		
	case "$escolha" in 
		1)	while : ; do
				escolha_fas=$(
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--ok-label "Selecionar" --cancel-label "Voltar ao Menu Inicial" \
					--title "Ferramentas de Administração de Servidores" \
					--menu "Escolha uma das ferramentas abaixo:" \
					0 0 0 \
					1 'OPENSSH-SERVER - Servidor para conexões SSH' \
					2 'LAMP SERVER - Apache/Mysql/PHP + PhpMyAdmin' \
					3 'POSTFIX - Servidor de E-mails' \
					4 'WEBMIN - Administração com interface web de Servidores' \
					5 'APACHE TOMCAT - Container Java Web' \
					6 'JAVA 7 - Oracle JDK7')
					
				#Se Usuário pressionar ESC ou selecionar 'Voltar' o programa volta para o menu principal
				[ $? -ne 0 ] && break
				
				case "$escolha_fas" in
					1)
						setOpenssh
					;;
					
					2) 
						setLAMPP						
					;;
					
					3) 
						setPostfix
					;;
					
					4) 
						setWebmin
					;;
					
					5)
						setTomcat
					;;
					
					6)
						setJava
					;;
				
				esac #Fim do case de Ferramentas Administrativas
			done #Fim do while de Ferramentas Administrativas
			;;
			
		2)	while : ; do
				escolha_fss=$(
					dialog --stdout  \
					--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
					--ok-label "Selecionar" --cancel-label "Voltar ao Menu Inicial" \
					--title "Ferramentas de Segurança de Servidores" \
					--menu "Escolha uma das ferramentas abaixo:" \
					0 0 0 \
					1 'NMAP - Scanner de Portas em um determinado host' \
					2 'CHKROOTKIT - Procura por Root Kits no Sistema')
				
				#Se Usuário pressionar ESC ou selecionar 'Voltar' o programa volta para o menu principal
				[ $? -ne 0 ] && break
				
				case "$escolha_fss" in
					1)
						nmapMenu
					;;
					
					2) 
						rootkitMenu						
					;;
				
				esac #Fim do case de Ferramentas de Segurança
			done #Fim do while de Ferramentas de Segurança
			;;
		
		3)
			autalizaRepo
		;;
			
		4) 
			sobre
		;;
		
		5) break 
		;;
		
	esac
done
dialog --stdout  \
	--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
	--title "Saindo do Sistema!" \
	--yesno "Deseja MANTER os arquivos de log gerados?" \
	0 0
if [ $? != 0 ]; then
	rm "AUTOMASHELL_"`date +%Y%m%d%H`".log"
	dialog --stdout  \
	--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
	--title "Saindo do Sistema!" \
	--msgbox "Arquivos de LOG APAGADOS!\nObrigado por utilizar o nosso Sistema!" \
	0 0
else
	dialog --stdout  \
	--backtitle "AutomaShell - Servidores GNU/Linux Debian Like" \
	--title "Saindo do Sistema!" \
	--msgbox "Arquivos de LOG mantidos\nObrigado por utilizar o nosso Sistema!" \
	0 0
	echo "INFO - "`date +%d/%m/%Y-%H:%M:%S`" - Saindo do Sistema" >> "AUTOMASHELL_"`date +%Y%m%d%H`".log"
fi
clear